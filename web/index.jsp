<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="Nathan Smith" name="author">
    <meta content="The 960 Grid System is an effort to streamline web development workflow." name="description">

    <title>DVCSWeb</title>

    <script>
        this.top.location !== this.location && (this.top.location = this.location);
    </script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.17.custom.min.js"></script>
    <script type="text/javascript">
        $(function(){


            // Dialog
            $('#dialog').dialog({
                autoOpen: false,
                width: 600,
                buttons: {
                    "Ok": function() {
                        $(this).dialog("close");
                    },
                    "Cancel": function() {
                        $(this).dialog("close");
                    }
                }
            });

            // Dialog Link
            $('#dialog_link').click(function(){
                $('#dialog').dialog('open');
                return false;
            });

            //hover states on the static widgets
            $('#dialog_link, ul#icons li').hover(
                function() { $(this).addClass('ui-state-hover'); },
                function() { $(this).removeClass('ui-state-hover'); }
            );

        });
    </script>

    <link type="text/css" href="css/start/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" media="all" href="css/reset.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="css/text.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="css/960.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="css/base.css"/>

    <style type="text/css">
        /*demo page css*/
        body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}
        .demoHeaders { margin-top: 2em; }
        #login_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
        #login_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
        #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
        #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
        ul#icons {margin: 0; padding: 0;}
        ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
        ul#icons span.ui-icon {float: left; margin: 0 4px;}
    </style>
</head>
<body>


<div class="container_12">
    <h1 class="grid_2 ">DVCSWeb</h1>
    <div class="grid_2 push_8">
        <a href="#" id="login_link" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-newwin"></span>Login</a>
    </div>
    <hr/>
    <p id="description" class="grid_4 prefix_4 suffix_4">A web interface for centralized DVCS servers.</p>

    <hr/>
    <div class="grid_12">

            <a href="#" id="dialog_link" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-newwin"></span>Create Repository</a>
            <%-----%>
            <%--<a href="#">Lorem</a>--%>

    </div>
</div>

</body>
</html>